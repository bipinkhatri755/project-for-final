<?php
$title = "Home Page";
include "header.php";

$movies_product = $db_handle->runQuery("SELECT * FROM products WHERE category = 'movie' ORDER BY id ASC LIMIT 3");
$books_product = $db_handle->runQuery("SELECT * FROM products WHERE category = 'book' ORDER BY id ASC LIMIT 3");

?>
	<div class="main">
		<div class="site-hero">
			<h1>BOOK & VIDEOS</h1>
			<a href="./products.php" class="btn">Shop Now</a>
		</div>
		<div class="features">
			<div class="f-item">
				<img src="assets/img/credit-card.png" alt="product image">
				<h3 class="title">Secured Payments</h3>
			</div>
			<div class="f-item">
				<img src="assets/img/package.png" alt="product image">
				<h3 class="title">Free shipping and Returns</h3>
			</div>
			<div class="f-item">
				<img src="assets/img/headphone.png" alt="product image">
				<h3 class="title">24/7 Customer Service</h3>
			</div>
		</div>
		<div class="new-movies">

			<h2>NEW MOVIES</h2>
			<?php
			if (!empty($movies_product)) { 
				foreach($movies_product as $key=>$value){
			?>
			<a href="./product-detail.php?product_id=<?php echo $movies_product[$key]["id"] ?>" class="item-card">
				<figure><img  src="<?php  if($movies_product[$key]["image"]){ echo $movies_product[$key]["image"];} else {echo 'assets/img/default-image.png';}; ?>" alt="product image"></figure>
				<h3><?php echo $movies_product[$key]["product_name"]; ?></h3>
				<p><?php echo "£".$movies_product[$key]["price"]; ?></p>
			</a>
			<?php
				}
			}else{
				?>
					<h3>No Product Yet.</h3>
				<?php
			}
			?>

			<a href="./products.php?category=movie" class="btn center-button">View More</a>
		</div>
		<div class="new-movies">
			<h2>NEW BOOKS</h2>
			<?php
			if (!empty($books_product)) { 
				foreach($books_product as $key=>$value){
			?>
			<a href="./product-detail.php?product_id=<?php echo $books_product[$key]["id"] ?>" class="item-card">
				<figure><img  src="<?php  if($books_product[$key]["image"]){ echo $books_product[$key]["image"];} else {echo 'assets/img/default-image.png';}; ?>" alt="product image"></figure>
				<h3><?php echo $books_product[$key]["product_name"]; ?></h3>
				<p><?php echo "£".$books_product[$key]["price"]; ?></p>
			</a>
			<?php
				}
			}else{
				?>
					<h3>No Product Yet.</h3>
				<?php
			}
			?>
			<a href="./products.php?category=book" class="btn center-button">View More</a>
		</div>
	</div>



	</div>
	
	<?php
include "footer.php";
?>
