<?php

$title = "Card Page";
include "header.php";


if (!isset($_SESSION['user_id'])) {
	
	header('Location: login.php');
	exit();
}


$query = "SELECT * FROM customers WHERE id = '" . $_SESSION['user_id'] ."'";
$current_user = $db_handle->runQuery($query);
$current_user  = $current_user[0];

?>


	<div class="main">
		<h2 class="title">
			Checkout
		</h2>
		<form class="half-content"  method="post" action="./process-payment.php">
			<div class="main-form-wrapper">
				<div class="main-form" id="main-form">

					<div class="input-wrapper">
						<label class="form-label" for="email">Email address </label>

						<input type="email" id="email" disabled value="<?php echo $current_user['email'];  ?>" class="form-control" />
					</div>
					<div class="input-wrapper">
						<label class="form-label" for="phone">Phone</label>

						<input type="text" id="phone" value="<?php echo $current_user['phone'];  ?>" name="phone" class="form-control" />
					</div>
					<div class="input-wrapper">
						<label class="form-label" for="Address">Shipping Address</label>

						<input type="text" id="Address" value="<?php echo $current_user['address'];  ?>" name="address" class="form-control" />
					</div>
					<div class="input-wrapper">
						<label class="form-label" for="zip">Zip code</label>

						<input type="text" id="zip" value="<?php echo $current_user['zip_code'];  ?>" name="zip_code" class="form-control" />
					</div>
				</div>
			</div>
			<div class="purchase-info">
				<h2 class="price">Total: <span>£<?php echo $_SESSION["cart_total_item"];?></span></h2>
				<p class="description">
					In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the
					visual
					form of a document or a typeface without relying on meaningful content.
				</p>
				<div class="card-form">

					<div class="main-form" id="card-form" >

						<div class="input-wrapper">
							<label class="form-label" for="card">Card Number*</label>

							<input type="text" id="card" class="form-control" />
						</div>
						<div class="half-form half-content">
							<div class="input-wrapper">
								<label class="form-label" for="expiry">Expiry*</label>

								<input type="text" id="expiry" placeholder="**/**" class="form-control" />
							</div>
							<div class="input-wrapper">
								<label class="form-label" for="Card">Card Code*</label>

								<input type="password" id="Card" placeholder="****" class="form-control" />
							</div>
						</div>

		
						<input type="submit"  class="btn sign-btn" value="Purchase">
</div>
				</div>
			</div>
</form>
	</div>



	</div>
	<?php
		include "footer.php";
	?>