<?php

session_start();
require_once("dbcontroller.php");
$db_handle = new DBController();

?>

<!DOCTYPE html>
<html>

<head>
	
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
	<link href="assets/css/style.css" rel="stylesheet">
</head>

<body>
	<header>

		<nav class="navbar">
			<h1 class="logo"><a class="navbar-brand" href="/">BOOK & VIDEO</a></h1>
			<ul class="nav">
				<li><a href="./home.php">Home</a></li>
				<li><a href="./products.php">Store</a></li>
				<?php if (!isset($_SESSION['user_id'])) { ?>
					<li><a href="./register.php">Register</a></li>
					<li><a href="./login.php">Login</a></li>
				<?php }else{ ?>
					<li><a href="./account.php">Account</a></li>
					<li><a href="./logout.php">Logout</a></li>
				<?php } ?>
			</ul>
			<div class="icons">
				<a href="./search.php">&#128269;</a>
				<a href="./cart-page.php">&#128722;</a>
			</div>
		</nav>
	</header>