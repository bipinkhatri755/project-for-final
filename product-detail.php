<?php
$title = "Product Detail";
include "header.php";
?>
	<div class="main">
		<h2 class="title">
		Product Detail
		</h2>
		<div class="half-content">
		<?php
			$product = $db_handle->runQuery("SELECT * FROM products WHERE id = " . $_GET["product_id"] . " ORDER BY id ASC");
			
			if (!empty($product)) { 
				$product = $product[0];
			
		?>
			<div class="img-wrapper">
				<img src="<?php echo $product["image"]; ?>" alt="product image">
			</div>
			<div class="purchase-info">
				<h2 class="price"><?php echo $product['product_name'] ?></h2>
				<h3 class="price">Price: <span>£<?php echo $product['price'] ?></span></h3>
				<p class="description">
				<?php echo $product['description'] ?>
				</p>
				<div class="card-form">

					<form method="post" class="main-form" action="./cart-page.php?action=add&code=<?php echo $product["id"]; ?>">
					<input type="hidden"  name="quantity" value="1" size="2" />
						<input type="submit" class="btn order-btn" value="Order">
					</form>
					
				</div>
			</div>
			<?php
			}
			?>
		</div>
	</div>



	</div>
	<?php
		include "footer.php";
	?>