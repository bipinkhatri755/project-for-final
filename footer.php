<footer class="main-footer">
		<div class="sec-1">Liverpool L69 3BX, United Kingdom</div>
		<div class="sec-2">
			<p>Email : info@book-video-shop.com</p>
			<p>Call: 1-100-000-0000</p>
		</div>
		<div class="sec-3">
			<ul>
				<li><a href="#">Shipping & Returns</a></li>
				<li><a href="#">FAQ</a></li>
			</ul>
		</div>
	</footer>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</body>

</html>