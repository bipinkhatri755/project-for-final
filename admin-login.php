<?php
$title = "Login Page";
include "header.php";

if(isset($_SESSION['admin_user_id'])) {
	header('Location: add-product.php');
	exit();
  }
  
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$email = $_POST['email'];
	$password = $_POST['password'];
	$current_user = $db_handle->runQuery("SELECT * FROM users WHERE email = '" . $email ."' ORDER BY id ASC");
	if ($current_user && count($current_user) > 0) {

		if(password_verify($password, $current_user[0]['password'])){
			$_SESSION['admin_user_id'] = $current_user[0]['id'];
			header('Location: add-product.php');
			exit();
		}else{
			echo 'Invalid password.';

		}
		
		

	} else {

		echo 'User not found.';

	}

}


?>
	<div class="main">
		<h2 class="title">
			Admin sign in
		</h2>
		<form class="main-form" method="post">
	
			<div class="input-wrapper">
				<label class="form-label" for="email">Email address</label>

				<input type="email" id="email" name="email" class="form-control" />
			</div>

		
			<div class="input-wrapper">
				<label class="form-label" for="password">Password</label>
				<input type="password" id="password" name="password" class="form-control" />

			</div>
	
			<input type="submit" class="btn sign-btn" value="Sign in">


	</form>
	</div>
	<?php
include "footer.php";
?>