<?php
class DBController {
	private $host = "localhost";
	private $user = "root";
	private $password = "";
	private $database = "video_book_shopping";
	private $conn;
	
	function __construct() {
		$this->conn = $this->connectDB();
	}
	
	function connectDB() {
		$conn = mysqli_connect($this->host,$this->user,$this->password,$this->database);
		return $conn;
	}
	
	function runQuery($query) {
		$result = mysqli_query($this->conn,$query);
		if($result != false){
			while($row=mysqli_fetch_assoc($result)) {
				$resultset[] = $row;
			}		
			if(!empty($resultset))
				return $resultset;
		}
		
	}
	
	function numRows($query) {
		$result  = mysqli_query($this->conn,$query);
		return $result;	
	}
	function insertWithId($query) {
		$result  = mysqli_query($this->conn,$query);
		$inserted_id = mysqli_insert_id($this->conn);
		return $inserted_id;	
	}
}
?>