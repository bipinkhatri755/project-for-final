<?php
$title = "Register Page";
include "header.php";


if(isset($_SESSION['user_id'])) {
	header('Location: account.php');
	exit();
  }
  

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$conn = $db_handle->connectDB();
	$username = $_POST['user_name'];
	$password = $_POST['password'];
	$email = $_POST['email'];
	$password_confirm =  $_POST['password_confirm'];
  
	if($password !== $password_confirm) {
	  echo 'Password does not match the confirm password.';
	} else {
	  	$hashed_password = password_hash($password, PASSWORD_DEFAULT);
	

	 	 $current_user = $db_handle->runQuery("SELECT * FROM customers WHERE email = '" . $email ."' ORDER BY id ASC");


		if(count($current_user) == 0){
		
			$query = "INSERT INTO customers (customer_name,email, password) VALUES ('$username','$email', '$hashed_password')";
			$result = $db_handle->numRows($query);
			if(!$result) {
				echo 'Error: ' . mysqli_error($conn);
			} else {
				header('Location: login.php');
				exit();
			}
		}else{
			echo "user has ben registred.";
			
		}
	}
  }
  ?>
<div class="main">
	<h2 class="title">
		Sign up
	</h2>
	<form class="main-form" method="post" >

		<div class="input-wrapper">
			<label class="form-label" for="user-name">Full Name</label>

			<input type="text" id="user-name" name="user_name" class="form-control" />
		</div>
		<div class="input-wrapper">
			<label class="form-label" for="email">Email address</label>

			<input type="email" id="email" name="email" class="form-control" />
		</div>


		<div class="input-wrapper">
			<label class="form-label" for="password">Password</label>
			<input type="password" id="password" name="password" class="form-control" />

		</div>

		<div class="input-wrapper">
			<label class="form-label" for="re-password">Re Password</label>
			<input type="password" id="re-password" name="password_confirm" class="form-control" />

		</div>

		<input type="submit" class="btn sign-btn" value="Sign up">
	</form>
</div>
<?php
include "footer.php";
?>