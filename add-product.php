<?php
$title = "Register Page";
include "header.php";



if(!isset($_SESSION['admin_user_id'])) {

	header('Location: admin-login.php');
	exit();
  }
  
  $success = "";

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	

    $product_name = $_POST["product_name"];
    $category = $_POST["category"];
    $description = $_POST["description"];
    $price = $_POST["price"];
    $is_digital = isset($_POST["is_digital"]) ? 1 : 0;
  

    $target_dir = "assets/uploads/";
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    $image_file_type = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);

    $query = "INSERT INTO products (product_name, category, description, image, price, is_digital) VALUES ('" . $product_name . "', '" . $category. "', '" . $description. "', '" . $target_file. "', '" . $price. "', '" . $is_digital. "')";
    $result = $db_handle->numRows($query);
    if($result == true){
        $success = "<p class='success'> Product Added successfully.</p>";
    }else{
        $success = "<p class='err'> Product Added Faild.</p>";
    }
  }
  ?>
<div class="main">
	<h2 class="title">
		Add new Product
	</h2>
    <?php
    if($success){
        echo $success;
    } ?>
	<form class="main-form" method="post" enctype="multipart/form-data">

		<div class="input-wrapper">
            <label for="product_name">Product Name:</label>
        <input type="text" id="product_name" name="product_name" required><br>

		</div>
		<div class="input-wrapper">
        <label for="category">Category:</label>
            <select id="category" name="category" required>
                <option value="">Select a category</option>
                <option value="movie">Movie</option>
                <option value="book">Book</option>
            </select>
		</div>


		<div class="input-wrapper">
            <label for="description">Description:</label>
            <textarea id="description" name="description" rows="5" required></textarea><br>
		</div>
        
        <div class="input-wrapper">
            <label for="image">Image:</label>
            <input type="file" id="image" name="image" accept="image/*" required><br>
		</div>

        <div class="input-wrapper">
             <label for="price">Price:</label>
            <input type="number" id="price" name="price" min="0.01" step="0.01" required><br>
		</div>


        <div class="input-wrapper">
            <label for="is_digital">Is it digital?</label>
            <input type="checkbox" id="is_digital" name="is_digital" value="yes"><br>
        </div>

		<input type="submit" class="btn sign-btn" value="Submit New Product">
	</form>
   
</div>
<?php
include "footer.php";
?>