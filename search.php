<?php
$title = "Login Page";
include "header.php";
?>
	<div class="main">
		<h2 class="title">
			Search Products
		</h2>
		<form class="main-form" method="get" action="./products.php">
	
			<div class="input-wrapper">
				<label class="form-label" for="search">Search your product</label>

				<input type="text" id="search" name="search" class="form-control" />
			</div>

			<input type="submit" class="btn sign-btn" value="Search Product">

	</form>
	</div>
	<?php
include "footer.php";
?>