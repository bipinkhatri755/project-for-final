<?php
$title = "Product Page";
include "header.php";


if(isset($_GET['category'])  && $_GET['category'] != ""){
	$categoy_name = $_GET['category'];
	$product_array = $db_handle->runQuery("SELECT * FROM products WHERE category = '" . $categoy_name . "' ORDER BY id ASC");
}else{
	$product_array = $db_handle->runQuery("SELECT * FROM products ORDER BY id ASC");
}




$search_flag = false;
if(isset($_GET['search'])  && $_GET['search'] != ""){
	$search_flag = true;
	$search_term = $_GET['search'];
	$results = array();
	foreach($product_array as $product) {
		if(stripos($product['product_name'], $search_term) !== false || stripos($product['description'], $search_term) !== false) {
			$results[] = $product;
		}
	}
	$product_array = $results;
}



?>
	<div class="main">
		<h2 class="title">
		Products
		</h2>
		<div class="products">
		<?php
			if (!empty($product_array)) { 
				foreach($product_array as $key=>$value){
		?>
			<a href="./product-detail.php?product_id=<?php echo $product_array[$key]["id"] ?>" class="item-card">
				<figure><img  src="<?php  if($product_array[$key]["image"]){ echo $product_array[$key]["image"];} else {echo 'assets/img/default-image.png';}; ?>" alt="product image"></figure>
				<h3><?php echo $product_array[$key]["product_name"]; ?></h3>
				<p><?php echo "£".$product_array[$key]["price"]; ?></p>
			</a>
			<?php
				}
			}else if($search_flag){
				?>
				<h3>No results found.</h3>
				<?php
			}else{
				?>
					<h3>No Product Yet.</h3>
				<?php
			}
			?>
		</div>
	</div>



	</div>
	
<?php
	include "footer.php";
?>
