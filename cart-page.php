<?php

$title = "Card Page";
include "header.php";

if(!empty($_GET["action"])) {
switch($_GET["action"]) {
	case "add":
		if(!empty($_POST["quantity"])) {
			$productByCode = $db_handle->runQuery("SELECT * FROM products WHERE id=" . $_GET["code"]);
			$itemArray = array($productByCode[0]["id"]=>array('product_id'=>$productByCode[0]["id"],'name'=>$productByCode[0]["product_name"], 'description'=>$productByCode[0]["description"], 'quantity'=>$_POST["quantity"], 'price'=>$productByCode[0]["price"], 'image'=>$productByCode[0]["image"]));

			if(!empty($_SESSION["cart_item"])) {
				
				
					$_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
				}
			} else {
				$_SESSION["cart_item"] = $itemArray;
			}
		}
	break;
	
	case "empty":
		unset($_SESSION["cart_item"]);
	break;	
}
}
?>

<body>
	<div id="shopping-cart">
		<div class="txt-heading">Shopping Cart</div>

		<a id="btnEmpty" href="./cart-page.php?action=empty">Empty Cart</a>
		<?php
		if(isset($_SESSION["cart_item"])){
			$total_quantity = 0;
			$total_price = 0;
		?>
		<table class="tbl-cart" cellpadding="10" cellspacing="1">
			<tbody>
				<tr>
					<th style="text-align:left;">Name</th>

					<th style="text-align:right;" width="5%">Quantity</th>
					<th style="text-align:right;" width="10%">Unit Price</th>
					<th style="text-align:right;" width="10%">Price</th>
					<th style="text-align:center;" width="5%">Remove</th>
				</tr>
				<?php		
    foreach ($_SESSION["cart_item"] as $item){
        $item_price = $item["quantity"]*$item["price"];
		?>
				<tr>
					<td><img src="<?php echo $item["image"]; ?>" class="cart-item-image" />
						<?php echo $item["name"]; ?>
					</td>
					<td style="text-align:right;">
						<?php echo $item["quantity"]; ?>
					</td>
					<td style="text-align:right;">
						<?php echo "£ ".$item["price"]; ?>
					</td>
					<td style="text-align:right;">
						<?php echo "£ ". number_format($item_price,2); ?>
					</td>
					<td style="text-align:center;"><a href="./cart-page.php?action=remove&code=<?php echo $item["product_id"]; ?>"
							class="btnRemoveAction"><img src="icon-delete.png" alt="Remove Item" /></a></td>
				</tr>
				<?php
				$total_quantity += $item["quantity"];
				$total_price += ($item["price"]*$item["quantity"]);
		}
		?>

				<tr>
					<td colspan="2" align="right">Total:</td>
					<td align="right">
						<?php echo $total_quantity; ?>
					</td>
					<td align="right" colspan="2"><strong>
							<?php  $_SESSION["cart_total_item"] = $total_price;  echo "£ ".number_format($total_price, 2); ?>
						</strong></td>

				</tr>
			</tbody>
		</table>
		<a style="width:100px;margin-right: 0;margin-left: auto;" href="./checkout.php" class="btn order-btn">Checkout</a>


		<?php
} else {
?>
		<div class="no-records">Your Cart is Empty</div>
		<?php 
}
?>
	</div>

</body>

</html>